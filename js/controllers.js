app
    .run(function ($rootScope, $state) {
        if (sessionStorage.vaccinesAuth) {
            $rootScope.authData = JSON.parse(sessionStorage.vaccinesAuth);
        } else {
            $rootScope.authData = undefined;
        }
        $rootScope.checkLogin = function () {
            if (!$rootScope.authData) {
                $state.go('login');
            }
            if($rootScope.authData.role == 'Center'){
                if($state.$current.name == 'doctors'){
                    $state.go('doctors',{id:$rootScope.authData.center.id});
                }else if($state.$current.name == 'update-doctors'){
                    // var data = {
                    //     type: 'add',
                    //     id: $rootScope.authData.center.id
                    // }
                    // $state.go('update-doctors', {
                    //     id: JSON.stringify(data)
                    // });
                }else{
                    $state.go('list-appointments');
                }
            }else{
                if($state.$current.name == 'list-appointments'){
                    $state.go('dashboard');
                }
            }
        }
        $rootScope.logout = function () {
            $rootScope.authData = undefined;
            sessionStorage.clear();
            window.location.href = '#/login';
        };

        $rootScope.successToast = function (msg) {
            $.toast({
                heading: '',
                text: msg,
                position: 'bottom-center',
                loaderBg: '#fff',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            })
        }
    })

    .controller('loginCtrl', function ($rootScope, $scope, http, $state) {
        if ($rootScope.authData) {
            $state.go('dashboard');
        }
        $scope.place = {
            lat: 12.923349869951434,
            lon: 77.62523305344234
        };
        $rootScope.currPage = $state.$current.name;
        $scope.login = function (user) {
            var data = new FormData();
            data.append('tag', 'login');
            data.append('mobile', user.uname);
            data.append('password', user.password);
            http.post(data).then(res => {
                if (res.data.error == 0) {
                    if (res.data.role == 'Admin') {
                        $scope.errFlg = false;
                        $scope.errMsg = '';
                        sessionStorage.vaccinesAuth = JSON.stringify(res.data);
                        $rootScope.authData = res.data;
                        $state.go('dashboard');
                    } else if (res.data.role == 'Center') {
                        $scope.errFlg = false;
                        $scope.errMsg = '';
                        sessionStorage.vaccinesAuth = JSON.stringify(res.data);
                        $rootScope.authData = res.data;
                        $state.go('list-appointments');
                    } else {
                        var msg = "Unable to login with provided credentials!";
                        $scope.errFlg = true;
                        $scope.errMsg = msg;
                    }
                } else {
                    $scope.errFlg = true;
                    $scope.errMsg = res.data.message;
                }
            }).catch(err => {
                console.log(err);
            })
        }
        $scope.goRegister = function(){
            $scope.registerFlg = true;
        }
        $scope.goLogin = function(id){
            if(id){
                $rootScope.successToast('Registration Completed');
            }
            $scope.registerFlg = false;
        }

        $scope.initMap = function (location) {
            let latLng = new google.maps.LatLng(location.lat, location.lon);
            let mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
            $scope.addMarker($scope.map)
            $scope.getCurrentAddress(latLng).then((res) => {
                $scope.place.address = res;
                document.getElementById('address').value = res;
            })

            // google.maps.event.addListenerOnce($scope.map, 'idle', () => {
            //   const event = new Event('resize');
            //   window.dispatchEvent(event);
            // });
        }

        $scope.getCurrentAddress = function (location) {
            return new Promise((resolve, reject) => {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'location': location
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        resolve(results[0].formatted_address)
                    } else {
                        reject(status);
                    }
                });
            })
        }

        $scope.addMarker = function (map) {
            var GoogleMapsEvent = google.maps.event;
            $scope.marker = new google.maps.Marker({
                map: map,
                animation: google.maps.Animation.DROP,
                position: $scope.map.getCenter(),
                draggable: true
            })
            $scope.marker.addListener('dragend', (event) => {
                $scope.place.lat = event.latLng.lat();
                $scope.place.lon = event.latLng.lng();
                $scope.getCurrentAddress(event.latLng).then((res) => {
                    $scope.place.address = res;
                    document.getElementById('address').value = res;
                })
            });
        }

        $scope.initPlaceSearch = function () {
            var input = document.getElementById('address');
            let autocomplete = new google.maps.places.Autocomplete(input, {
                types: ["address"],
            });
            autocomplete.addListener("place_changed", () => {
                //get the place result
                let place = google.maps.places.PlaceResult = autocomplete.getPlace();

                //verify result
                if (place.geometry === undefined || place.geometry === null) {
                    return;
                }
                setTimeout(() => {
                    $scope.place.lat = place.geometry.location.lat();
                    $scope.place.lon = place.geometry.location.lng();
                    $scope.map.setCenter(place.geometry.location)
                    $scope.marker.setPosition(place.geometry.location);
                    if (place.formatted_address) {
                        $scope.place.address = place.formatted_address;
                    }
                }, 0);
            });
        }

        setTimeout(() =>{
            $scope.initMap($scope.place);
            $scope.initPlaceSearch();
        }, 200);

        $scope.addCenter = function (center) {
            if (center.name && center.uname && center.mobile && $('#address').val() && center.password) {
                var data = new FormData();
                data.append('tag', 'add_center');
                data.append('user_password', center.password);
                data.append('name', center.name);
                data.append('address', $('#address').val());
                data.append('user_name', center.uname);
                data.append('user_mobile', center.mobile);
                data.append('latitude', $scope.place.lat);
                data.append('longitude', $scope.place.lon);
                http.post(data).then(res => {
                    if(res.data.error == 1){
                        $rootScope.successToast(res.data.message);
                    }else{
                        $scope.goLogin(1);
                    }
                })
            }
        }
    })

    .controller('dashboardCtrl', function ($rootScope, $scope, http, $state) {
        $rootScope.checkLogin();
        $rootScope.currPage = $state.$current.name;
        var postdata = new FormData();
        postdata.append('tag', 'dashboard');
        http.post(postdata).then(res => {
            $scope.dashboard = res.data;
        })

        $scope.usersList = [];
        var data = new FormData();
        data.append('tag', 'users');
        http.post(data).then(res => {
            var cnt = 0;
            res.data.forEach((val, key) => {
                if (val.role == 'User') {
                    cnt++;
                    val.index = cnt;
                    $scope.usersList.push(val);
                }
            })
        })
    })

    .controller('vaccinesCtrl', function ($rootScope, $scope, http, $state) {
        $rootScope.checkLogin();
        $rootScope.currPage = $state.$current.name;
        $scope.loadVaccines = function () {
            $scope.vaccines = [];
            var data = new FormData();
            data.append('tag', 'vaccines');
            http.post(data).then(res => {
                $scope.vaccines = res.data;
            });
        };

        $scope.getWeek = function (week) {
            if (week == -1) return "-";
            else if (week == 0) return "Birth";
            else {
                var dosage = "";
                var year = parseInt(week / 48);
                if (year == 1) dosage += "1 year";
                else if (year > 1) dosage += year + " years";
                var month = parseInt((week % 48) / 4);
                if (month == 1) dosage += " 1 month";
                else if (month > 1) dosage += " " + month + " months";
                week = parseInt(week % 4);
                if (week == 1) dosage += " 1 week";
                else if (week > 1) dosage += " " + week + " weeks";
                return dosage.trim();
            }
        }
        $scope.loadVaccines();
        $scope.goDelete = function (id) {
            $scope.currentId = id;
            $('#deleteModal').modal('show');
        }

        $scope.deleteVaccine = function () {
            var data = new FormData();
            data.append('tag', 'delete_vaccine');
            data.append('id', $scope.currentId);
            http.post(data).then(res => {
                $rootScope.successToast(res.data.message);
                $scope.loadVaccines();
            });
        }

        $scope.goDetail = function (vacci) {
            $state.go('add-vaccines', {
                id: JSON.stringify(vacci)
            });
        }
    })

    .controller('addVaccinesCtrl', function ($rootScope, $scope, http, $state, $stateParams) {
        $rootScope.checkLogin();
        $rootScope.currPage = 'vaccines-list';
        $scope.vacciDetail = $stateParams.id == 'add' ? undefined : JSON.parse($stateParams.id);
        $scope.vaccine = {};
        $scope.isEdit = false;
        if ($scope.vacciDetail) {
            $scope.isEdit = true;
            $scope.vaccines = [];
            $scope.vaccine.name = $scope.vacciDetail.name;
            $scope.vaccine.prevents = $scope.vacciDetail.prevents;
            if ($scope.vacciDetail.dose_1 != -1) {
                $scope.vaccines.push({
                    dose: $scope.vacciDetail.dose_1
                });
            }
            if ($scope.vacciDetail.dose_2 != -1) {
                $scope.vaccines.push({
                    dose: $scope.vacciDetail.dose_2
                });
            }
            if ($scope.vacciDetail.dose_3 != -1) {
                $scope.vaccines.push({
                    dose: $scope.vacciDetail.dose_3
                });
            }
            if ($scope.vacciDetail.dose_4 != -1) {
                $scope.vaccines.push({
                    dose: $scope.vacciDetail.dose_4
                });
            }
            if ($scope.vacciDetail.dose_5 != -1) {
                $scope.vaccines.push({
                    dose: $scope.vacciDetail.dose_5
                });
            }
            if ($scope.vaccines.length == 0) {
                $scope.vaccines = [{
                    dose: ''
                }];
            }
        } else {
            $scope.isEdit = false;
            $scope.vaccines = [{
                dose: ''
            }];
        }
        $scope.addDose = function (dose, index) {
            if ($scope.vaccines[index].dose) {
                $scope.vaccines.push({
                    dose: ''
                });
            }
        };

        $scope.deleteDose = function (index) {
            if ($scope.vaccines.length > 1) {
                $scope.vaccines.splice(index, 1);
            }
        };

        $scope.addVaccine = function (vacci) {
            var data = new FormData();
            if ($scope.vacciDetail) {
                data.append('tag', 'edit_vaccine');
                data.append('id', $scope.vacciDetail.id);
            } else {
                data.append('tag', 'add_vaccine');
            }
            data.append('name', vacci.name);
            data.append('prevents', vacci.prevents);
            $scope.vaccines.forEach(function (val, key) {
                if (val.dose) {
                    data.append('dose_' + (key + 1), val.dose);
                }
            });
            if ($scope.vaccines.length == 1) {
                $scope.doseFlg = $scope.vaccines[0].dose ? false : true;
            }
            if (!$scope.doseFlg) {
                http.post(data).then(res => {
                    if (!$scope.vacciDetail) {
                        $state.go('vaccines-list');
                    }
                    $rootScope.successToast(res.data.message);
                })
            }
        }
    })

    .controller('usersCtrl', function ($rootScope, $scope, http, $state) {
        $rootScope.checkLogin();
        $rootScope.currPage = $state.$current.name;
        $scope.usersList = [];
        var data = new FormData();
        data.append('tag', 'users');
        http.post(data).then(res => {
            var cnt = 0;
            res.data.forEach((val, key) => {
                if (val.role == 'User') {
                    cnt++;
                    val.index = cnt;
                    $scope.usersList.push(val);
                }
            })
        });
    })

    .controller('centersCtrl', function ($rootScope, $scope, http, $state) {
        $rootScope.checkLogin();
        $rootScope.currPage = $state.$current.name;
        $scope.centersList = [];
        $scope.goDetail = function (center) {
            var params = {
                id: center.id,
                name: center.name
            }
            $state.go('center-detail', {
                id: JSON.stringify(params)
            });
        }
        $scope.listCenters = function(){
            var data = new FormData();
            data.append('tag', 'centers');
            http.post(data).then(res => {
                $scope.centersList = res.data;
            });
        };

        $scope.listCenters();
        

        $scope.deleteModal = function (id) {
            $scope.docId = id;
            $('#deleteModal').modal('show');
        }

        $scope.deleteCenter = function (id) {
            var postdata = new FormData();
            postdata.append('tag', 'delete_center');
            postdata.append('id', $scope.docId);
            http.post(postdata).then(res => {
                $scope.listCenters();
                $rootScope.successToast(res.data.message);
            })
        }
    })

    .controller('addCentersCtrl', function ($rootScope, $scope, http, $state, $stateParams) {
        $rootScope.checkLogin();
        $rootScope.currPage = 'centers';
        $scope.pageId = $stateParams.id;
        $scope.usersList = [];
        $scope.place = {
            lat: 12.923349869951434,
            lon: 77.62523305344234
        };
        if ($scope.pageId != 'add') {
            var postdata = new FormData();
            postdata.append('tag', 'centers');
            http.post(postdata).then(res => {
                res.data.forEach((val, key) => {
                    if (val.id == $scope.pageId) {
                        $scope.center = {
                            name: val.name,
                            uname: val.user_name,
                            mobile: val.user_mobile,
                        };
                        $scope.place = {
                            lat: val.latitude,
                            lon: val.longitude,
                            address: val.address
                        };
                        $('#address').val(val.address);
                        $scope.usersList.forEach((value, key) => {
                            if (val.user_id == value.id) {
                                $('user').val(JSON.stringify(value));
                            }
                        })
                    }
                });
                setTimeout(function () {
                    $scope.initMap($scope.place);
                    $scope.initPlaceSearch();
                }, 200);
            })
        } else {
            setTimeout(function () {
                $scope.initMap($scope.place);
                $scope.initPlaceSearch();
            }, 200);
        }

        $scope.addCenter = function (center) {
            if (center.name && center.uname && center.mobile && $('#address').val()) {
                var data = new FormData();
                if ($scope.pageId == 'add') {
                    data.append('tag', 'add_center');
                    if (!center.password) {
                        return;
                    }
                    data.append('user_password', center.password);
                } else {
                    data.append('tag', 'edit_center');
                    data.append('id', $scope.pageId);
                }
                data.append('name', center.name);
                data.append('address', $('#address').val());
                data.append('user_name', center.uname);
                data.append('user_mobile', center.mobile);
                data.append('latitude', $scope.place.lat);
                data.append('longitude', $scope.place.lon);
                http.post(data).then(res => {
                    if(res.data.error == 1){
                        $rootScope.successToast(res.data.message);
                    }else{
                        $state.go('centers');
                    }
                })
            }
        }

        $scope.initMap = function (location) {
            let latLng = new google.maps.LatLng(location.lat, location.lon);
            let mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
            $scope.addMarker($scope.map)
            $scope.getCurrentAddress(latLng).then((res) => {
                $scope.place.address = res;
                document.getElementById('address').value = res;
            })

            // google.maps.event.addListenerOnce($scope.map, 'idle', () => {
            //   const event = new Event('resize');
            //   window.dispatchEvent(event);
            // });
        }

        $scope.getCurrentAddress = function (location) {
            return new Promise((resolve, reject) => {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'location': location
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        resolve(results[0].formatted_address)
                    } else {
                        reject(status);
                    }
                });
            })
        }

        $scope.addMarker = function (map) {
            var GoogleMapsEvent = google.maps.event;
            $scope.marker = new google.maps.Marker({
                map: map,
                animation: google.maps.Animation.DROP,
                position: $scope.map.getCenter(),
                draggable: true
            })
            $scope.marker.addListener('dragend', (event) => {
                $scope.place.lat = event.latLng.lat();
                $scope.place.lon = event.latLng.lng();
                $scope.getCurrentAddress(event.latLng).then((res) => {
                    $scope.place.address = res;
                    document.getElementById('address').value = res;
                })
            });
        }

        $scope.initPlaceSearch = function () {
            var input = document.getElementById('address');
            let autocomplete = new google.maps.places.Autocomplete(input, {
                types: ["address"],
            });
            autocomplete.addListener("place_changed", () => {
                //get the place result
                let place = google.maps.places.PlaceResult = autocomplete.getPlace();

                //verify result
                if (place.geometry === undefined || place.geometry === null) {
                    return;
                }
                setTimeout(() => {
                    $scope.place.lat = place.geometry.location.lat();
                    $scope.place.lon = place.geometry.location.lng();
                    $scope.map.setCenter(place.geometry.location)
                    $scope.marker.setPosition(place.geometry.location);
                    if (place.formatted_address) {
                        $scope.place.address = place.formatted_address;
                    }
                }, 0);
            });
        }
    })

    .controller('centerDetailCtrl', function ($rootScope, $scope, http, $state, $stateParams) {
        $rootScope.checkLogin();
        $rootScope.currPage = 'centers';
        $scope.params = JSON.parse($stateParams.id);
        $scope.centerId = $scope.params.id;

        $scope.getCheckAll = function (isChecked) {
            $scope.vaccines.forEach(val => {
                val.checked = isChecked;
            });
        };

        $scope.updateProvided = function () {
            // var vacciProvided = [];
            var vacciData = new FormData();
            vacciData.append('tag', 'update_vaccines_provided');
            vacciData.append('center_id', $scope.centerId);
            $scope.vaccines.forEach(val => {
                if (val.checked) {
                    vacciData.append('vaccines[]', val.id);
                }
            });
            http.post(vacciData).then(res => {
                $rootScope.successToast(res.data.message);
            })
        }

        var data = new FormData();
        data.append('tag', 'vaccines');
        http.post(data).then(vacci => {
            $scope.vaccines = vacci.data;
            var postdata = new FormData();
            postdata.append('tag', 'vaccines_provided');
            postdata.append('center_id', $scope.centerId);
            http.post(postdata).then(res => {
                $scope.vaccines.forEach(val => {
                    res.data.forEach(pro => {
                        if (pro.vaccine_id == val.id) {
                            val.checked = true;
                        }
                    })
                });
            })
        })

    })

    .controller('centerProfileCtlr', function ($rootScope, $scope, http, $state, $stateParams) {
        $rootScope.currPage = $state.$current.name;
        $scope.userDetail = {};
        var data = new FormData();
        data.append('tag', 'centers');
        http.post(data).then(res => {
            res.data.forEach((val,key) =>{
                if(val.user_id == $rootScope.authData.id){
                    $scope.userDetail = {
                        uname : val.user_name,
                        mobile : val.user_mobile
                    }
                }
            });
            
        });
        $scope.updateUser = function(user){
            var data = new FormData();
            data.append('tag', 'edit_center_user');
            data.append('user_id', $rootScope.authData.id);
            data.append('user_name', $scope.userDetail.uname);
            data.append('user_mobile', $scope.userDetail.mobile);
            data.append('user_password', $scope.userDetail.password);
            http.post(data).then(res => {
                $rootScope.successToast(res.data.message);
            });
        };
    })

    .controller('appointmentsCtrl', function ($rootScope, $scope, http, $state, $stateParams) {
        $scope.centerId = $stateParams.id;
        $rootScope.currPage = $state.$current.name;

        $rootScope.checkLogin();
        $scope.role = 'Center'
        $scope.appointments = [];
        $scope.loadAppointments = function () {
            var postdata = new FormData();
            postdata.append('tag', 'centers');
            http.post(postdata).then(res => {
                res.data.forEach(val => {
                    if (val.user_id == $rootScope.authData.id) {
                        var data = new FormData();
                        data.append('tag', 'appointments');
                        data.append('center_id', val.id);
                        http.post(data).then(res => {
                            $scope.appointments = res.data;
                        })
                    }
                })
            });
        };
        $scope.loadAppointments();

        $scope.updateAppoint = function (id, status) {
            var postdata = new FormData();
            postdata.append('tag', 'update_appointment');
            postdata.append('id', id);
            postdata.append('status', status);
            http.post(postdata).then(res => {
                $scope.loadAppointments();
                $rootScope.successToast(res.data.message);
            })
        }
    })

    .controller('doctorsCtrl', function ($rootScope, $scope, http, $state, $stateParams) {
        $rootScope.currPage = $state.$current.name;
        $scope.params = $stateParams.id;
        $rootScope.checkLogin();
        $scope.role = 'Admin';

        $scope.doctorsList = [];
        $scope.centersList = [];
        $scope.listDoctors = function (id) {
            var postdata = new FormData();
            postdata.append('tag', 'doctors');
            postdata.append('center_id', id);
            http.post(postdata).then(res => {
                console.log(res);
                $scope.doctorsList = res.data;
            });
        };

        var postdata = new FormData();
        postdata.append('tag', 'centers');
        http.post(postdata).then(res => {
            $scope.centersList = res.data;
            if ($scope.centersList.length > 0) {
                if($scope.params == 'list'){
                    $scope.centerId = res.data[0].id;
                    $scope.listDoctors(res.data[0].id);
                }
            }
        });

        if($scope.params != 'list'){
            $scope.centerId = $scope.params;
            $scope.listDoctors($scope.centerId);
        };

        $scope.changeCenter = function (id) {
            $scope.listDoctors(id);
        }

        $scope.addDoctors = function (id) {
            var data = {
                type: 'add',
                id: id
            }
            $state.go('update-doctors', {
                id: JSON.stringify(data)
            });
        };

        $scope.editDoctors = function (doctor) {
            doctor.type = 'edit';
            $state.go('update-doctors', {
                id: JSON.stringify(doctor)
            });
        }

        $scope.deleteModal = function (id) {
            $scope.docId = id;
            $('#deleteModal').modal('show');
        }

        $scope.deleteDoctor = function (id) {
            var postdata = new FormData();
            postdata.append('tag', 'delete_doctor');
            postdata.append('id', $scope.docId);
            http.post(postdata).then(res => {
                $scope.listDoctors($scope.centerId);
                $rootScope.successToast(res.data.message);
            })
        }

    })

    .controller('doctorsAddCtrl', function ($rootScope, $scope, http, $state, $stateParams) {
        $scope.details = JSON.parse($stateParams.id);
        $scope.doctor = {};
        $scope.isEdit = false;
        if ($scope.details.type == 'add') {
            $scope.centerId = $scope.details.id;
            $scope.isEdit = false;
        } else {
            $scope.isEdit = true;
            $scope.doctor = $scope.details;
            $scope.doctorId = $scope.details.id;
        }
        $rootScope.currPage = 'doctors';

        $rootScope.checkLogin();
        $scope.role = 'Admin';
        $scope.addDoctor = function (doctor) {
            var postdata = new FormData();
            if ($scope.isEdit) {
                postdata.append('tag', 'edit_doctor');
                postdata.append('id', $scope.doctorId);
            } else {
                postdata.append('tag', 'add_doctor');
                postdata.append('center_id', $scope.centerId);
            }
            postdata.append('name', doctor.name);
            postdata.append('phone', doctor.phone);
            http.post(postdata).then(res => {
                if (!$scope.isEdit) {
                    $state.go('doctors',{id:$scope.centerId});
                }
                $rootScope.successToast(res.data.message);
            });
        }
    })

    .controller('feedsCtrl', function ($rootScope, $scope, http, $state, $stateParams) {
        $rootScope.currPage = $state.$current.name;
        $rootScope.checkLogin();
        $scope.role = 'Admin';

        $scope.feedsList = [];
        $scope.listFeeds = function (id) {
            var postdata = new FormData();
            postdata.append('tag', 'feeds');
            http.post(postdata).then(res => {
                console.log(res);
                $scope.feedsList = res.data;
            });
        };
        $scope.listFeeds();

        $scope.addFeeds = function (id) {
            var data = {
                type: 'add',
            }
            $state.go('update-feeds', {
                id: JSON.stringify(data)
            });
        };

        $scope.editFeeds = function (doctor) {
            doctor.type = 'edit';
            $state.go('update-feeds', {
                id: JSON.stringify(doctor)
            });
        }

        $scope.deleteModal = function (id) {
            $scope.docId = id;
            $('#deleteModal').modal('show');
        }

        $scope.deleteFeed = function (id) {
            var postdata = new FormData();
            postdata.append('tag', 'delete_feed');
            postdata.append('id', $scope.docId);
            http.post(postdata).then(res => {
                $scope.listFeeds();
                $rootScope.successToast(res.data.message);
            })
        }

    })

    .controller('feedsAddCtrl', function ($rootScope, $scope, http, $state, $stateParams) {
        $scope.details = JSON.parse($stateParams.id);
        console.log($scope.details)
        $scope.feed = {};
        $rootScope.currPage = 'feeds';
        $scope.isEdit = false;
        if ($scope.details.type == 'add') {
            $scope.isEdit = false;
        } else {
            $scope.isEdit = true;
            $scope.feed = $scope.details;
        }
        $('#file').change((evt) =>{
            var imageUrl = URL.createObjectURL($(evt.target)[0].files[0]);
            prevImages(imageUrl);
        });
        function prevImages(url){
            $('.img').remove();
            $('.imag_cls').append(`<img class="img" src="${url}">`);
        }
        if($scope.details.image){
            var url = "http://vaccine.glivade.in/" + $scope.details.image;
            prevImages(url);
        }
        $rootScope.checkLogin();
        $scope.role = 'Admin';
        $scope.addFeed = function (doctor) {
            var postdata = new FormData();
            if ($scope.isEdit) {
                postdata.append('tag', 'edit_feed');
                postdata.append('id', $scope.feed.id);
            } else {
                postdata.append('tag', 'add_feed');
            }
            if($('#file')[0].files[0]){
                postdata.append('image', $('#file')[0].files[0]);
            }
            postdata.append('title', doctor.title);
            postdata.append('description', doctor.description);
            http.post(postdata).then(res => {
                if (!$scope.isEdit) {
                    $state.go('feeds',{id:'list'});
                }
                $rootScope.successToast(res.data.message);
            });
        }
    })

    