var app = angular.module('vaccines', ['ui.router'])
    .config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
        function auth($state, $rootScope) {
         
        }

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'login.html',
                controller: 'loginCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'dashboard.html',
                controller: 'dashboardCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('vaccines-list', {
                url: '/vaccines-list',
                templateUrl: 'vaccines-list.html',
                controller: 'vaccinesCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('add-vaccines', {
                url: '/add-vaccines/:id',
                templateUrl: 'add-vaccines.html',
                controller: 'addVaccinesCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('users', {
                url: '/users-list',
                templateUrl: 'users.html',
                controller: 'usersCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('centers', {
                url: '/centers',
                templateUrl: 'centers.html',
                controller: 'centersCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('center', {
                url: '/center/:id',
                templateUrl: 'add-centers.html',
                controller: 'addCentersCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('center-detail', {
                url: '/center-detail/:id',
                templateUrl: 'center-detail.html',
                controller: 'centerDetailCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('center-userprofile', {
                url: '/center-userprofile',
                templateUrl: 'center-userprofile.html',
                controller: 'centerProfileCtlr',
                resolve: {
                    auth: auth
                }
            })

            .state('list-appointments', {
                url: '/list-appointments',
                templateUrl: 'list-appointments.html',
                controller: 'appointmentsCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('doctors', {
                url: '/list-doctors/:id',
                templateUrl: 'doctors.html',
                controller: 'doctorsCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('update-doctors', {
                url: '/update-doctors/:id',
                templateUrl: 'addedit-doctors.html',
                controller: 'doctorsAddCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('feeds', {
                url: '/list-feeds/:id',
                templateUrl: 'feeds.html',
                controller: 'feedsCtrl',
                resolve: {
                    auth: auth
                }
            })

            .state('update-feeds', {
                url: '/update-feeds/:id',
                templateUrl: 'addedit-feeds.html',
                controller: 'feedsAddCtrl',
                resolve: {
                    auth: auth
                }
            })

        $locationProvider.hashPrefix('');
        $urlRouterProvider.otherwise('/login');
    })

    .service('http', function ($http) {
        var APIURL = 'http://vaccine.glivade.in/api.php';
        this.post = function (data) {
            $http.defaults.headers.post['Content-Type'] = undefined;
            return $http.post(APIURL, data);
        }
    })