<?php

header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

function getBaseUrl() {
	$protocol = ($_SERVER['HTTPS'] && ($_SERVER['HTTPS'] != "off")) ? "https" : "http";
	return $protocol . "://" . $_SERVER['HTTP_HOST'] . "/";
}

function sendNotification($fcmToken, $appointment) {
	$api_key = 'AAAArSYJbqE:APA91bFP7uLV--xJWz0e4XghIADf5vB66GsnckLkbddyz51pMpzpvOlxjrLwMsjN4m5z2hkLFrKBpMP6lgxJ8MOpwPeYgy2EDkfwk68DO1TN4ocJp0VveKKqBY_U7GhJmN05871xK0b5';
	$headers = array('Authorization: key=' . $api_key, 'Content-Type: application/json');
	$msg = array('title' => 'Appointment '. $appointment['status'],
		'description' => 'Your child ' . $appointment['name'] . '\'s appointment fixed on '
		. date_format(date_create($appointment['appointment_at']), "d-m-Y H:i a") . ' is ' . $appointment['status']);
	$fields = array('to' => $fcmToken, 'data' => $msg);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	curl_exec($ch);
	curl_close($ch);
}

if (isset($_POST['tag']) && $_POST['tag'] != '') {

	$tag = $_POST['tag'];

	$con = mysqli_connect('localhost', 'vaccine', 'Vaccine@2k18', 'vaccine') or die ("Could not connect to database");

	$response = array();

	if($tag == "register") {
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$mobile = $_POST['mobile'];
		$password = md5($_POST['password']);
		$email = mysqli_real_escape_string($con, $_POST['email']);
		$address = mysqli_real_escape_string($con, $_POST['address']);

		$query1 = "INSERT INTO user(name, mobile, password, email, address) VALUES('$name', '$mobile', '$password', '$email', '$address')";
		$result1 = mysqli_query($con, $query1);

		if($result1) {

			$id = mysqli_insert_id($con);

			$query2 = "SELECT id, role, name FROM user WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			if($result2){
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Registered successfully!";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Already registered!";
			echo json_encode($response);
		}
	}

	if($tag == "login") {
		$mobile = $_POST['mobile'];
		$password = md5($_POST['password']);

		$query = "SELECT id, role, name FROM user WHERE mobile = '$mobile' AND BINARY password = '$password'";
		$result = mysqli_query($con, $query);

		$count = mysqli_num_rows($result);

		if($count == 1) {
			$response = mysqli_fetch_assoc($result);
			if ($response['role'] == 'Center') {
				$userId = $response['id'];
				$query2 = "SELECT * FROM vaccine_center WHERE user_id = '$userId'";
				$result2 = mysqli_query($con, $query2);
				mysqli_close($con);
				$center = mysqli_fetch_assoc($result2);
				$response['center'] = $center;
			} else {
				mysqli_close($con);
			}
			$response["error"] = 0;
			$response["message"] = "Welcome ".$response["name"];
			echo json_encode($response);
		} else {
			mysqli_close($con);
			$response["error"] = 1;
			$response["message"] = "Unable to login with provided credentials!";
			echo json_encode($response);
		}
	}

	if($tag == "update_fcm") {
		$id = $_POST['id'];
		$fcmToken = mysqli_real_escape_string($con, $_POST['fcm_token']);

		$query = "UPDATE user SET fcm_token = '$fcmToken' WHERE id = '$id'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			$response["error"] = 0;
			$response["message"] = "FMC Token updated successfully";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "dashboard") {
		$query = "SELECT * FROM (SELECT COUNT(*) AS user_count FROM user WHERE role = 'User') AS user,
		(SELECT COUNT(*) AS center_count FROM user WHERE role = 'Center') AS center,
		(SELECT COUNT(*) AS children_count FROM children) AS children,
		(SELECT COUNT(*) AS vaccine_count FROM vaccine) AS vaccine,
		(SELECT COUNT(*) AS appointment_count FROM appointment) AS appointment";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			$response = mysqli_fetch_assoc($result);
		}

		echo json_encode($response);
	}

	if($tag == "users") {
		$query = "SELECT * FROM user WHERE role = 'User'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		echo json_encode($response);
	}

	if($tag == "vaccines") {
		$query = "SELECT * FROM vaccine";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		echo json_encode($response);
	}

	if($tag == "add_vaccine") {
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$prevents = mysqli_real_escape_string($con, $_POST['prevents']);
		$dose1 = $_POST['dose_1'];
		$dose2 = $_POST['dose_2'];
		$dose3 = $_POST['dose_3'];
		$dose4 = $_POST['dose_4'];
		$dose5 = $_POST['dose_5'];

		$query1 = "INSERT INTO vaccine(name, prevents, dose_1, dose_2, dose_3, dose_4, dose_5) VALUES('$name', '$prevents', '$dose1', '$dose2', '$dose3', '$dose4', '$dose5')";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$id = mysqli_insert_id($con);

			$query2 = "SELECT * FROM vaccine WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Vaccine added successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "edit_vaccine") {
		$id = $_POST['id'];
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$prevents = mysqli_real_escape_string($con, $_POST['prevents']);
		$dose1 = $_POST['dose_1'];
		$dose2 = $_POST['dose_2'];
		$dose3 = $_POST['dose_3'];
		$dose4 = $_POST['dose_4'];
		$dose5 = $_POST['dose_5'];

		$query1 = "UPDATE vaccine SET name = '$name', prevents = '$prevents', dose_1 = '$dose1', dose_2 = '$dose2', dose_3 = '$dose3', dose_4 = '$dose4', dose_5 = '$dose5' WHERE id = '$id'";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$query2 = "SELECT * FROM vaccine WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Vaccine edited successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "delete_vaccine") {
		$id = $_POST['id'];

		$query = "DELETE FROM vaccine WHERE id = '$id'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Vaccine deleted successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "centers") {
		$query = "SELECT vaccine_center.*, user.name as user_name, user.mobile as user_mobile FROM vaccine_center JOIN user ON user.id = vaccine_center.user_id";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		echo json_encode($response);
	}

	if($tag == "add_center") {
		$userName = mysqli_real_escape_string($con, $_POST['user_name']);
		$userMobile = $_POST['user_mobile'];
		$userPassword = md5($_POST['user_password']);
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$address = mysqli_real_escape_string($con, $_POST['address']);
		$latitude = $_POST['latitude'];
		$longitude = $_POST['longitude'];

		$query1 = "INSERT INTO user(role, name, mobile, password) VALUES('Center', '$userName', '$userMobile', '$userPassword')";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$id = mysqli_insert_id($con);

			$query2 = "INSERT INTO vaccine_center(user_id, name, address, latitude, longitude) VALUES('$id', '$name', '$address', '$latitude', '$longitude')";
			$result2 = mysqli_query($con, $query2);

			if($result2) {
				$id = mysqli_insert_id($con);

				$query3 = "SELECT vaccine_center.*, user.name as user_name, user.mobile as user_mobile FROM vaccine_center JOIN user ON user.id = vaccine_center.user_id WHERE vaccine_center.id = '$id'";
				$result3 = mysqli_query($con, $query3);

				mysqli_close($con);

				$count = mysqli_num_rows($result3);

				if($count == 1) {
					$response = mysqli_fetch_assoc($result3);
					$response["error"] = 0;
					$response["message"] = "Vaccination center registered successfully";
					echo json_encode($response);
				} else {
					$response["error"] = 1;
					$response["message"] = "Error occurreds! Try again!";
					echo json_encode($response);
				}
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "edit_center") {
		$id = $_POST['id'];
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$address = mysqli_real_escape_string($con, $_POST['address']);
		$latitude = $_POST['latitude'];
		$longitude = $_POST['longitude'];

		$query1 = "UPDATE vaccine_center SET name = '$name', address = '$address', latitude = '$latitude', longitude = '$longitude' WHERE id = '$id'";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$query2 = "SELECT * FROM vaccine_center WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Vaccine center edited successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "edit_center_user") {
		$userId = $_POST['user_id'];
		$userName = mysqli_real_escape_string($con, $_POST['user_name']);
		$userMobile = $_POST['user_mobile'];
		$userPassword = md5($_POST['user_password']);

		$query1 = "UPDATE user SET name = '$userName', mobile = '$userMobile', password = '$userPassword' WHERE id = '$userId'";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$query2 = "SELECT role, id, name, mobile FROM user WHERE id = '$userId'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Vaccine center user edited successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "vaccines_provided") {
		$centerId = $_POST['center_id'];

		$query = "SELECT * FROM vaccine_provided WHERE center_id = '$centerId'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		echo json_encode($response);
	}

	if($tag == "update_vaccines_provided") {
		$centerId = $_POST['center_id'];
		$vaccines = $_POST['vaccines'];

		$query1 = "DELETE FROM vaccine_provided WHERE center_id = '$centerId'";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			for ($i = 0; $i < count($vaccines); $i++) {
				$vaccine = $vaccines[$i];
				$query2 = "INSERT INTO vaccine_provided(center_id, vaccine_id) VALUES('$centerId', '$vaccine')";
				mysqli_query($con, $query2);
			}

			mysqli_close($con);

			$response["error"] = 0;
			$response["message"] = "Vaccination provided updated successfully";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "delete_center") {
		$id = $_POST['id'];

		$query = "DELETE FROM vaccine_center WHERE id = '$id'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Vaccine center deleted successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "children") {
		$userId = $_POST['user_id'];

		$query = "SELECT * FROM children WHERE user_id = '$userId'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		echo json_encode($response);
	}

	if($tag == "add_child") {
		$userId = $_POST['user_id'];
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$gender = $_POST['gender'];
		$dob = $_POST['dob'];

		$query1 = "INSERT INTO children(user_id, name, gender, dob) VALUES('$userId', '$name', '$gender', '$dob')";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$id = mysqli_insert_id($con);

			$query2 = "SELECT * FROM children WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Child added successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "edit_child") {
		$id = $_POST['id'];
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$gender = $_POST['gender'];
		$dob = $_POST['dob'];

		$query1 = "UPDATE children SET name = '$name', gender = '$gender', dob = '$dob' WHERE id = '$id'";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$query2 = "SELECT * FROM children WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Child edited successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "delete_child") {
		$id = $_POST['id'];

		$query = "DELETE FROM children WHERE id = '$id'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Child deleted successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "reviews") {
		$centerId = $_POST['center_id'];

		$query = "SELECT review.*, user.name, user.mobile FROM review LEFT JOIN user ON user.id = review.user_id WHERE center_id = '$centerId'";
		$result = mysqli_query($con, $query);

		$reviews = array();
		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$reviews[] = $row;
			}
		}
		$response["reviews"] = $reviews;

		mysqli_close($con);

		echo json_encode($response);
	}

	if($tag == "center_reviews") {
		$userId = $_POST['user_id'];
		$centerId = $_POST['center_id'];

		$query1 = "SELECT review.*, user.name, user.mobile FROM review LEFT JOIN user ON user.id = review.user_id WHERE center_id = '$centerId' AND user_id <> '$userId'";
		$result1 = mysqli_query($con, $query1);

		$reviews = array();
		if($result1) {
			while($row = mysqli_fetch_assoc($result1)) {
				$reviews[] = $row;
			}
		}
		$response["reviews"] = $reviews;

		$query2 = "SELECT review.*, user.name, user.mobile FROM review LEFT JOIN user ON user.id = review.user_id WHERE center_id = '$centerId' AND user_id = '$userId'";
		$result2 = mysqli_query($con, $query2);

		if($result2) {
			$response["my_review"] = mysqli_fetch_assoc($result2);
		}

		mysqli_close($con);

		echo json_encode($response);
	}

	if($tag == "add_review") {
		$userId = $_POST['user_id'];
		$centerId = $_POST['center_id'];
		$rating = $_POST['rating'];
		$review = mysqli_real_escape_string($con, $_POST['review']);

		$query = "INSERT INTO review(user_id, center_id, rating, review, last_updated) VALUES('$userId', '$centerId', '$rating', '$review', NOW())";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Review added successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Already reviewed!";
			echo json_encode($response);
		}
	}

	if($tag == "edit_review") {
		$id = $_POST['id'];
		$rating = $_POST['rating'];
		$review = mysqli_real_escape_string($con, $_POST['review']);

		$query = "UPDATE review SET rating = '$rating', review = '$review', last_updated = NOW() WHERE id = '$id'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Review edited successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "delete_review") {
		$id = $_POST['id'];

		$query = "DELETE FROM review WHERE id = '$id'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Review removed successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}


	if($tag == "appointments") {
		$centerId = $_POST['center_id'];

		$query = "SELECT appointment.*, children.name, children.gender, children.dob, user.name as user_name FROM appointment LEFT JOIN children ON children.id = appointment.child_id LEFT JOIN user ON user.id = appointment.user_id WHERE appointment.center_id = '$centerId'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		echo json_encode($response);
	}

	if($tag == "my_centers") {
		$childId = $_POST['child_id'];
		$latitude = $_POST['latitude'];
		$longitude = $_POST['longitude'];

		$query = "SELECT vaccine_center.*, user.name as user_name, user.mobile as user_mobile, (SELECT AVG(rating) FROM review WHERE review.center_id = vaccine_center.id) AS rating,(6371 * acos(cos(radians('$latitude')) * cos(radians(vaccine_center.latitude)) * cos(radians(vaccine_center.longitude) - radians('$longitude')) + sin (radians('$latitude')) * sin( radians(vaccine_center.latitude)))) AS distance FROM vaccine_center JOIN user ON user.id = vaccine_center.user_id ORDER BY distance";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		echo json_encode($response);
	}

	if($tag == "my_appointments") {
		$userId = $_POST['user_id'];

		$query = "SELECT appointment.*, vaccine_center.name, vaccine_center.address, vaccine_center.latitude, vaccine_center.longitude, children.name as child_name FROM appointment LEFT JOIN vaccine_center ON vaccine_center.id = appointment.center_id LEFT JOIN children ON children.id = appointment.child_id WHERE appointment.user_id = '$userId' ORDER BY appointment.id";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		echo json_encode($response);
	}

	if($tag == "request_appointment") {
		$userId = $_POST['user_id'];
		$centerId = $_POST['center_id'];
		$childId = $_POST['child_id'];
		$appointmentAt = $_POST['appointment_at'];

		$query = "INSERT INTO appointment(user_id, center_id, child_id, appointment_at, requested_at) VALUES('$userId', '$centerId', '$childId', '$appointmentAt', NOW())";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Appointment requested successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "cancel_appointment") {
		$id = $_POST['id'];

		$query = "DELETE FROM appointment WHERE id = '$id'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Appointment cancelled successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "update_appointment") {
		$id = $_POST['id'];
		$status = $_POST['status'];

		$query1 = "UPDATE appointment SET status = '$status' WHERE id = '$id'";
		$result1 = mysqli_query($con, $query1);

		$query2 = "SELECT appointment.*, children.name, children.gender, children.dob, user.name as user_name FROM appointment LEFT JOIN children ON children.id = appointment.child_id LEFT JOIN user ON user.id = appointment.user_id WHERE appointment.id = '$id'";
		$result2 = mysqli_query($con, $query2);

		$appointment = mysqli_fetch_assoc($result2);
		$userId = $appointment['user_id'];

		$query3 = "SELECT * FROM user WHERE id = '$userId'";
		$result3 = mysqli_query($con, $query3);

		$user = mysqli_fetch_assoc($result3);

		if ($user['fcm_token']) {
			sendNotification($user['fcm_token'], $appointment);
		}

		mysqli_close($con);

		if($result1){
			$response["error"] = 0;
			$response["message"] = "Appointment updated successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "doctors") {
		$centerId = $_POST['center_id'];

		$query = "SELECT * FROM doctor WHERE center_id = '$centerId'";
		$result = mysqli_query($con, $query);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		mysqli_close($con);

		echo json_encode($response);
	}

	if($tag == "add_doctor") {
		$centerId = $_POST['center_id'];
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$phone = $_POST['phone'];

		$query1 = "INSERT INTO doctor(center_id, name, phone) VALUES($centerId, '$name', '$phone')";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$id = mysqli_insert_id($con);

			$query2 = "SELECT * FROM doctor WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Doctor added successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "edit_doctor") {
		$id = $_POST['id'];
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$phone = $_POST['phone'];

		$query1 = "UPDATE doctor SET name = '$name', phone = '$phone' WHERE id = '$id'";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$query2 = "SELECT * FROM doctor WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Doctor edited successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "delete_doctor") {
		$id = $_POST['id'];

		$query = "DELETE FROM doctor WHERE id = '$id'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Doctor deleted successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "feeds") {
		$query = "SELECT * FROM feed";
		$result = mysqli_query($con, $query);

		if($result) {
			while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
			}
		}

		mysqli_close($con);

		echo json_encode($response);
	}

	if($tag == "add_feed") {
		$title = mysqli_real_escape_string($con, $_POST['title']);
		$description = mysqli_real_escape_string($con, $_POST['description']);
		$image = null;

		if(isset($_FILES['image'])){
			$file_name = $_FILES['image']['name'];
			$file_tmp = $_FILES['image']['tmp_name'];
			$upload_dir = "uploads";
			if(is_dir($upload_dir) == false){
				mkdir($upload_dir);
			}
			$url = "";
			if(is_dir($upload_dir."/".$file_name) == false){
				$url = $upload_dir."/".$file_name;
				move_uploaded_file($file_tmp, $url);
			} else {									
				$url = $upload_dir."/".$file_name.time();
				rename($file_tmp, $url) ;				
			}
			$image = $url;
		}

		$query1 = "INSERT INTO feed(title, description, image, created_at) VALUES('$title', '$description', '$image', NOW())";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$id = mysqli_insert_id($con);

			$query2 = "SELECT * FROM feed WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Feed added successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "edit_feed") {
		$id = $_POST['id'];
		$title = mysqli_real_escape_string($con, $_POST['title']);
		$description = mysqli_real_escape_string($con, $_POST['description']);

		$query1 = "UPDATE feed SET title = '$title', description = '$description' WHERE id = '$id'";

		if(isset($_FILES['image'])){
			$file_name = $_FILES['image']['name'];
			$file_tmp = $_FILES['image']['tmp_name'];
			$upload_dir = "uploads";
			if(is_dir($upload_dir) == false){
				mkdir($upload_dir);
			}
			$url = "";
			if(is_dir($upload_dir."/".$file_name) == false){
				$url = $upload_dir."/".$file_name;
				move_uploaded_file($file_tmp, $url);
			} else {									
				$url = $upload_dir."/".$file_name.time();
				rename($file_tmp, $url) ;				
			}
			$query1 = "UPDATE feed SET title = '$title', description = '$description', image = '$url' WHERE id = '$id'";
		}

		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$query2 = "SELECT * FROM feed WHERE id = '$id'";
			$result2 = mysqli_query($con, $query2);

			mysqli_close($con);

			$count = mysqli_num_rows($result2);

			if($count == 1) {
				$response = mysqli_fetch_assoc($result2);
				$response["error"] = 0;
				$response["message"] = "Feed edited successfully";
				echo json_encode($response);
			} else {
				$response["error"] = 1;
				$response["message"] = "Error occurred! Try again!";
				echo json_encode($response);
			}
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}

	if($tag == "delete_feed") {
		$id = $_POST['id'];

		$query = "DELETE FROM feed WHERE id = '$id'";
		$result = mysqli_query($con, $query);

		mysqli_close($con);

		if($result){
			$response["error"] = 0;
			$response["message"] = "Feed deleted successfully!";
			echo json_encode($response);
		} else {
			$response["error"] = 1;
			$response["message"] = "Error occurred! Try again!";
			echo json_encode($response);
		}
	}
} else {
	$response["error"] = 1;
	$response["error_msg"] = "Required parameter 'tag' is missing!";
	echo json_encode($response);
}
?>
